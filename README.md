![Mapfit logo](mapfit-logo-md.png)

### Mapfit conversion tool

This tool helps convert styles developed for the Mapbox platform to the Mapfit/Mapzen platform.

There are two parts to this:

1. Converting the style from the [Mapbox Streets](https://www.mapbox.com/vector-tiles/mapbox-streets-v7) schema to the [Mapzen Vector Tiles](https://mapzen.com/documentation/vector-tiles/layers/) schema.
2. Converting from [Mapbox-GL-JS style spec](https://www.mapbox.com/mapbox-gl-js/style-spec/) (JSON) to a [Tangram scene file](https://mapzen.com/documentation/tangram/Scene-file/) (YAML).

### Using the comparison tool

1. `npm install -g budo`
2. `npm install`
3. `npm run start`
4. Open the URL printed out in the browser. You can add "#filename" to load a different file on the Tangram (right) side.
5. Click features on either side to interrogate the style.
6. Pan and zoom on the Mapbox (left) side to synchronise with the right.

#### Limitations

- Exponential zoom functions supported as series of linear approximations.
- `minzoom` supported only in one or two special cases.
- `*-opacity` only works if it can be combined into a color spec.
- Several parameters related to orientation of text/icons won't work properly.
- `fill-extrusion` layers are very very minimally supported.
- (not complete)

#### Not supported

- Property and zoom-and-property functions
- `maxzoom`
- shields
- Layer types: `raster`, `fill-extrusion` (could be done with "extrude: true", `heatmap`
- icons
- expressions
- These style properties:
  - `*-blur`
  - `fill-antialias`
  - `fill-offset`
  - `fill-pattern`
  - `icon-optional`
  - `line-gap-width`
  - `line-translate`
  - `line-round-limit`
  - `line-offset`
  - `line-pattern`
  - `text-line-height`
  - `text-letter-spacing`
  - `text-max-angle`
  - `text-translate-anchor`

- (not complete)

#### About

This open source tool was sponsored by Mapfit.