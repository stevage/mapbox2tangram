/* jshint esnext:true */

// a simple script to convert a file or two to YAML on disk.

var mb2mz = require('./mb2mz');
var schemaCross = require('./schemaCross');
let yaml = require('js-yaml');
function processStyle(filename, outname='out') {
    function copy(o) {
        return JSON.parse(JSON.stringify(o));
    }
    let style =schemaCross.convertSchema('mapbox', 'mapzen', require(filename));

    // first copy avoids messing up source object. second strips out undefined values.
    
    let output = copy(mb2mz.toTangram(copy(style)));
    require('fs').writeFileSync(`./compare/${outname}.yaml`, yaml.safeDump(output));
    console.log(`Wrote ./compare/${outname}.yaml`);
    require('fs').writeFileSync('./compare/in.json', JSON.stringify(style,undefined,4));
}

// processStyle('./test/mapfit-day.json', 'mapfit-day');
processStyle('./test/min-service.json', 'min-service');
// processStyle('./test/medieval.json', 'medieval');
