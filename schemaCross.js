/* jshint esnext:true */
/*
TODO:
- automatically find relevant source layer names by examining .sources {}
✓ iterate through each (possibly nested) filter of each layer, looking for keys that match
- handle OpenMapTiles "park" layer which is used unfiltered, but must be filtered in say mapzen landuse class=park
-- need to define it as park:* on the openmaptiles end
-- detect that, generate a filter that wasn't there previously.
- handle one layer (eg, OMT "motorway" which maps to motorway, motorway_link)
- Some fields have text-substitution in them, like icon-image: "default_{ref_length}"

- taxiway


General issues:
- Mapbox has #rail_station_label to use
- Mapbox uses integer admin_levels (4) not strings ("4")
- Mutually exclusive filters can be generated, should be resolved. Eg, mapbox, class=motorway, class=link
- Mapzen combines all water types which is really hard to deal with.
-- maybe we should add extra $type=polygon anytime there's a fill layer type.

Notes:
- Mapbox Streets v9 doesn't seem to have documentation. So layers like "landcover" aren't currently supported.


x = x = x
x = x = y
(x + n) = z = a  //eg (footway and cycleway) = path
(x && y) = z = a 
x = y = <not there>


x:a:b = y = z:f:g

u<int> = u<string> = u<?>
*/


/*
Simple two way mapping cases to handle:
layer key value: l k v

l:k=v  -> l:k=v             // simple case
l:k=*  -> l:k=*             // values same, just change key name
l:*=*  -> l:*=*             // same key and value in a different layer? unlikely but...

l      -> l:k=v             // eg, #aeroway -> roads:kind=aeroway

l:k=v  -> l                 // k=v doesn't exist, so don't worry
l:k=v  -> ''                // that data doesn't exist, don't walk to anything
l:k=v  -> l1:k1=v1,k2=v2    // eg, aeroway:kind_detail=runway implies roads:kind=aeroway + kind_detail=runway 
l:k1=v2,k2=v2 -> l:k=v      // maybe. especially if k2=$type



*/

/*
TODO:
- Mapbox scalerank
    #country_label 1-6
    #place_label 0-9
    #poi_label 1-5
    #airport_label 1-4

    Sort of maps to Mapzen min_zoom

- Mapbox layer
    #roads

    Sort of maps to Mapzen sort_rank

- Mapbox #barrier_line
*/



// engine-level property, prefer integers. 

let console = require('logging').default('schema-cross');
console.log = console.info;


const schemaNames = { mapbox: 0, mapzen: 1, openmaptiles: 2 };
const defined = x => x !== undefined;

// TODO define behaviour of _ and *

// in theory each can match as a regex, but we can only really handle '.*'
const crosswalk = [
    ['road:structure=tunnel', 'roads:is_tunnel=true', 'transportation:brunnel=tunnel'],
    ['road:structure=bridge', 'roads:is_bridge=true', 'transportation:brunnel=bridge'],
    //['road:class=motorway','roads:kind_detail=motorway','transportation:class=motorway'],
    ['road:class=link','roads:is_link=true','transportation:ramp=1'],
    ['road:class=major_rail','roads:kind=rail','transportation:class=rail'],
    ['road:class=minor_rail','roads:kind=tram','transportation:class=transit'], // eek

    ['road:class=street','roads:kind=minor_road','transportation:class=minor'],
    ['road:class=path','roads:kind=path','transportation:class=path'],
    ['road:class=ferry','roads:kind=ferry','transportation:class=ferry'], // Openmaptiles TBC
    ['road:type=.*','roads:kind_detail=.*'],
    ['road:class=.*','roads:kind_detail=.*','transportation:class=.*'],
    
    ['road_label:class=street','roads:kind=minor_road','transportation_name:class=minor'],
    ['road_label:class=path','roads:kind=path','transportation_name:class=path'],
    ['road_label:class=ferry','roads_label:kind=ferry','transportation_name:class=ferry'], // Openmaptiles TBC
    ['road_label:class=.*', 'roads:kind_detail=.*', 'transportation_name:class=.*'], // going from mapzen will be hard
    ['landuse:class=residential','landuse:kind=residential','landuse:class=residential'],
    ['landuse:class=glacier','landuse:kind=glacier','landcover:subclass=glacier'], // not documented, but in Kbasic style
    ['landuse:class=glacier','landuse:kind=glacier','landcover:subclass=ice_shelf'], // not documented, but in Kbasic style
    ['landuse:class=park','landuse:kind=park', 'park:class=nature_reserve'],
    ['landuse:class=.*','landuse:kind=.*','landuse:class=.*'],
    ['landuse:class=.*','landuse:kind=.*','landcover:class=.*'],

    // there's also #road:kind=pier in Mapzen
    ['barrier_line:class=land','landuse:kind=pier'],

    ['building:height=.*','buildings:height=.*','building:render_height=.*'],
    ['building:type=building:part','buildings:kind=building_part', 'building:class=buliding_part'],//don't think OMT one exists
    ['building:_=.*','buildings:_=.*', 'building:_=.*'],
    ['aeroway:_=.*','roads:kind=aeroway'],
    ['aeroway:type=runway', 'roads:kind_detail=runway', 'aeroway:class=runway'], // should have a kind=aeroway for mapzen
    ['aeroway:type=taxiway', 'roads:kind_detail=taxiway', 'aeroway:class=taxiway'],
    ['aeroway:type=apron', 'roads:kind_detail=apron', 'aeroway:class=apron'],
    ['place_label:type=suburb','places:kind=neighbourhood', 'place:class=suburb'],
    ['place_label:type=.*','places:kind_detail=.*', 'place:class=.*'],
    ['place_label:localrank=1','places:min_zoom=10'], // totally guessing here
    ['place_label:localrank=2','places:min_zoom=11'],
    ['place_label:localrank=3','places:min_zoom=12'],
    ['place_label:localrank=4','places:min_zoom=13'],
    ['place_label:localrank=5','places:min_zoom=14'],
    ['place_label:localrank=6','places:min_zoom=15'],
    ['poi_label:type=.*', 'pois:kind_detail=.*', 'poi:class=.*'],
    
    //massive TODO. 
    ['poi_label:scalerank=0','pois:min_zoom=8',''],
    ['poi_label:scalerank=1','pois:min_zoom=10',''],
    ['poi_label:scalerank=2','pois:min_zoom=12',''],
    ['poi_label:scalerank=3','pois:min_zoom=14',''],

    ['state_label:=', 'places:kind_detail=state'], // see also special behaviour
    ['country_label:=', 'places:kind=country'], // see also special behaviour

    // mapbox doesn't seem to have suburb boundaries?
    // also mapbox boundaries don't work cause admin levels have to be integers not strings
    ['admin:admin_level=.*', 'boundaries:kind_detail=.*', 'boundary:admin_level=.*'],
    // untested
    ['admin:maritime=1', 'boundaries:maritime_boundary=true', 'boundary:maritime=1'],
    ['admin:maritime=0', 'boundaries:maritime_boundary=false', 'boundary:maritime=0'],
    ['water:_=.*', 'water:$type=Polygon','water:class=*'],
    ['water_label:name=.*', 'water:name=.*','TODO'],
    ['water_label:area=.*', 'water:area=.*'],
    ['waterway:class=.*', 'water:kind=.*', 'waterway:class=.*'], // mapzen will cause problems with water bodies
    ['water:_=.*', 'water:kind=.*','water:class=.*'] // mapbox water doesn't have classes. TODO

];
/*
Given a source layer ("aeroway") and key/value(type=taxiway), look it up and return a layer/key/value triple.
*/
// TODO support 1->many manppings: 'roads:kind=aeroway&kind_detail=taxiway'
function walkKeyValue(layerName, key, value) {
    let fromIdx = schemaNames[fromSchema], toIdx = schemaNames[toSchema];
    // 
    let mapping;
    // TODO this line fails with mapbox://styles/stevage/cim5tjgwe00kta0m406yx7kea
    const stripKey = s => s.replace(/:.*$/, '');
    // console.info(layerName, key, value);
    const lkvStr = layerName + ':' + key + '=' + value;
    if (key) {
        // mapping = crosswalk.find(x => (new RegExp(x[fromIdx])).test(`${layerName}:${key}=${value}`));

        mapping = crosswalk.find(x => lkvStr.match(x[fromIdx]));
        // console.info(mapping);
    } else {
        // no key/value to filter on? Just grab the first layer that matches at all.
        mapping = crosswalk.find(x => stripKey(x[fromIdx]) === stripKey(layerName) );

    }
    // console.log(layerName, key, value, mapping);
    if (!mapping) {
        //console.log('NA ' + `${layerName}:${key}=${value}`);
        return undefined;
    }

    //LKV = layer, key, value
    let targetLKV = mapping[toIdx].split(/[:=]/);
    if (targetLKV[2].match(/\*/)) {
        if (value) {
            // map foo:bar -> baz:bar if the pattern is foo:* -> baz:*
            targetLKV[2] = targetLKV[2].replace('.*', value);
        } else { 
            // we landed on a wildcard replacement but we have nothing to replace it with
            console.warn("No value to choose for wildcard replacement: ", targetLKV);
            return [targetLKV[0]];
        }
    }
    const targetLKstr = targetLKV[0] + ':' + targetLKV[1];
    if (targetLKstr.match(/^(boundaries:kind_detail|water:area)$/) || 
        targetLKV[1].match(/^(admin_level|min_zoom|scalerank|(render_)?height)$/)) {
        // convert certain layers to numbers
        targetLKV[2] = +targetLKV[2];
        if (targetLKstr === 'water:area') {
            targetLKV[2] *= 1000; // I have no idea what the right ratio is.
        }
    }
    return targetLKV;
}
// Given a layer and its filter, return the layer name that it maps onto.
function targetLayer(sourceLayer, filter) {
    //if (sourceLayer === 'road_label')
    //console.log('!!!',[from,to,sourceLayer,filter]);
    let lkv;
    if (filter) {
        let op = filter[0], key = filter[1], value = filter[2];
        if (op === 'all' || op === 'any')
            // grab the first mapped layer that results in a value.
            return filter.slice(1).reduce(((l, f) => l || targetLayer(sourceLayer, f)), undefined);
        // this is a bit lazy on 'in', might fail if the first 'in' value is unrecognised
        if(op.match(/^(==|!=|in|!in|<|<=|>|>=)$/) && key !== '$type') {
            lkv = walkKeyValue(sourceLayer, key, value);            
        }
    } 

    if (!lkv) {
    // no filter, or one we can't handle? take a bigger guess
        lkv = walkKeyValue(sourceLayer);
    }
    return lkv && lkv[0];
    //return undefined;

}
/*
Recursively translate a single filter, which means translating all the layers and keys/values referred to.
*/
function walkFilter(sourceLayer, filter) {
    // all -> map individual components
    // == $type -> leave unchanged
    // in -> map each element

    if (filter === undefined) {
        // here the source layer had no filter but we think the target should have one.
        let w = walkKeyValue(sourceLayer);
        if (w && w.length == 3)
            return ['==', w[1], w[2]];
        else {
            console.log('No filter for ', sourceLayer, ' -> ', w);
            return undefined;
        }
    }
    let op = filter[0], key = filter[1], value = filter[2], subfilters = filter.slice(1), values = filter.slice(2);

    if (op === 'all') {
        if (filter.length === 1)
            console.log([sourceLayer,filter]);
        // if a filter doesn't map to anything, just remove it
        return ['all', ...subfilters.map(f => walkFilter(sourceLayer, f)).filter(defined) ];
    } else if (op === 'in') {
        // play it safe by converting 'in' to lots if independent matches, in case the key isn't the same
        return ['any', ...values.map(v => walkFilter(sourceLayer, ['==', key, v])).filter(defined)];
    } else if (op === '!in') {
        return ['all', ...values.map(v => walkFilter(sourceLayer, ['!=', key, v])).filter(defined)];
    } else if (op.match(/^(==|!=|<|>|<=|>=)$/)) {
        if (key === '$type')
            return filter;
        let w = walkKeyValue(sourceLayer, key, value);
        if (!w)
            return undefined;
        return [op, w[1], w[2]];

    } else {
        // We got nothing at all? Um, keep the source filter and hope for the best??
        console.warn('Filter mapped to nothing. Layer "', sourceLayer, '" may not appear. (', filter, ')');
        //console.log(filter);

        if (sourceLayer.match('state-label')) { //??
            return filter;
        } else {
            //return undefined;
        }
        // Hard choice. Whichever way we go (return undefined or filter) we may show layers that shouldn't, 
        // depending if in "where type != blah" or "where type = blah".

        //return undefined;
        return filter;
    }
}

// Map a layer by finding the target source-layer, and mapping the filter if any.
function walkLayer(layer) {
   if (Array.isArray(layer.filter) && layer.filter.length < 3) {            
        layer.filter = undefined; // defective filter in some liberty layers
    }

    if (layer.source === undefined) { // background layer            
        return layer;
    } else if (layer.source !== fromSchema && layer.source !== 'composite') {
        console.warn('Warning: layer with source ' + layer.source);
    }
    //console.log(`${l['source-layer']}:${layer.filter[1]}=${layer.filter[2]}`);

    let tl = targetLayer(layer['source-layer'], layer.filter);
    if (!tl) {
        console.warn('Dropping layer ', layer.id, layer['source-layer'],  layer.filter);
        //console.debug(, );
        return;
    }
    let outLayer = JSON.parse(JSON.stringify(layer));
    outLayer.source = toSchema; // ! conflating our name with internal source name
    outLayer['source-layer'] = tl;
    
    outLayer.filter = walkFilter(layer['source-layer'], layer.filter, layer.id); // undefined layer.filter is ok

    if (fromSchema === 'mapbox' && toSchema === 'mapzen') {
        const extraFilter = {
            'state_label': ['==', 'kind_detail', 'state'],
            'country_label': ['==',' kind','country'],
            'aeroway': ['==', 'kind','aeroway']
        }[layer['source-layer']];
        if (extraFilter) {
            outLayer.filter = ['all', extraFilter, outLayer.filter];
        }

    }

    return outLayer;


//console.log(`--> ${tl}:${tf[1]}=${tf[2]}`);
    //console.log(`--> ${tl}:${JSON.stringify(tf)}`);
    
}

function schemaSource() {
    let source = {
        mapzen: { tiles: ['http://tile.mapzen.com/mapzen/vector/v1/all/{z}/{x}/{y}.mvt?api_key=vector-tiles-LM25tq4'] },
        mapbox: { url: 'mapbox://mapbox.mapbox-streets-v7' },
        openmaptiles: { url: 'https://free.tilehosting.com/data/v3.json?key=tXiQqN3lIgskyDErJCeY' }
    }[toSchema];

    source.type = 'vector';
    return source;
}

let fromSchema, toSchema;

function convertSchema(from, to, style, customLogger) {
    if (customLogger) {
        console = customLogger;
    }
    fromSchema = from;
    toSchema = to;
    let output = JSON.parse(JSON.stringify(style));
    output.layers = style.layers.map(l => walkLayer(l)).filter(defined);
    output.sources = {
        [toSchema]: schemaSource(toSchema)
    };
    //console.log(JSON.stringify(output, undefined, 4));
    //console.info(output);
    return output;
}

function processStyle(fromSchema, toSchema, style, name='out') {
    let output = convertSchema(fromSchema, toSchema, style);
    require('fs').writeFileSync(`./out/${name}-${toSchema}.json`, JSON.stringify(output, undefined, 4));

}

module.exports = { convertSchema: convertSchema };