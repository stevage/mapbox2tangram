/* jshint esnext:true */
var should = require('should');

var mb2mz = require('../mb2mz');
var schemaCross = require('../schemaCross');

var nonLogger = {
    warn: _ => console.warn,
    info: _ => _,
    error: _ => _,
    debug: _ => _
};

function toTangram(style) {
    return mb2mz.toTangram(style, false, nonLogger);

}

function copy(o) {
    return JSON.parse(JSON.stringify(o));
}

function minimb() {
    return {
        version: 8,
        sources: {},
        layers: []
    };
}

function miniWithFilter(filter) {
    var m = minimb();
    m.layers.push({
        id: 'dots',
        type: 'circle',
        source: 'points',
        filter: filter
    });
    return m;
}

function walkStyle(l) {
    l.id = l.id || 'test';
    var m = minimb();
    m.layers.push(l);
    outl = toTangram(m).styles[l.id].draw;
    //console.log(outl);
    return outl;
}

function walkLayer(l) {
    l.id = l.id || 'test';
    var m = minimb();
    m.layers.push(l);
    outl = toTangram(m).layers[l.id];
    return outl;
}


function walkFilter(filter) {    
    return walkLayer({ id: 'dots', type: 'circle', source: 'points', filter: filter }).filter;
}

describe('Mapfit-Day', function() {
    var inStyle = require('./mapfit-day.json');
    var crossedStyle = schemaCross.convertSchema('mapbox', 'mapzen', inStyle, nonLogger);
    var outStyle = copy(toTangram(copy(crossedStyle)));

    describe('converted style', function() {
        it('should not have a "background" layer', function() {
            outStyle.layers.should.not.have.property('background');
        });
    });
});

describe('generic', function() {
    beforeEach(function() {
        s = minimb();
    });
    it('== maps to key:val', function() {
        walkFilter(['==', 'name', 'Lucy']).should.deepEqual({name: 'Lucy'});        
    });
    it('!in is handled correctly', function() {
        walkFilter(['!in', 'class', 'major_road','minor_road'])
        .should.deepEqual({not: { class: [ 'major_road', 'minor_road'] }});        
    });
    it('==,$type,LineString ==> $geometry:line', function() {
        walkFilter(['==', '$type', 'LineString']).should.deepEqual({$geometry: 'line'});
    });

    it('line of color and width', function() {
        lines = walkStyle({
            type: 'line',
            paint: {
                'line-width': 4,
                'line-color': 'blue',
            }
        });
        lines.color.should.equal('blue');
        lines.width.should.equal('4px');
    });
    it('text with offset', function() {
        text = walkStyle({
            type: 'symbol',
            paint: {
                'text-field': '{name_en}',
                'text-offset': [3, 3],
                'text-padding': 4
            }
        });
        text.text_source.should.equal('name');
        text.offset.should.deepEqual(['3px', '3px']);
        text.buffer.should.deepEqual(['4px', '4px']);
    });
    it('line-opacity and line-color as constants', function() {
        lines = walkStyle({
            type: 'line',
            paint: {
                'line-width': 4,
                'line-color': 'blue',
                'line-opacity': 0.3,
            }
        });
        lines.color.should.equal('rgba(0, 0, 255, 0.3)');
    });
    it('line-opacity function, line-color constant', function() {
        lines = walkStyle({
            type: 'line',
            paint: {
                'line-width': 4,
                'line-color': '#fff',
                'line-opacity': {
                    stops: [
                        [10, 0.2],
                        [12, 1]
                    ]
                }
            }
        });
        lines.color.should.deepEqual([
            [10, 'rgba(255, 255, 255, 0.2)'],
            [11, 'rgba(255, 255, 255, 0.6)'],
            [12, 'rgb(255, 255, 255)']
        ]);
    });
    it('line-opacity constant, line-color function', function() {
        lines = walkStyle({
            type: 'line',
            paint: {
                'line-width': 4,
                'line-color': {
                    stops: [
                        [10, 'blue'],
                        [15, 'white']
                    ]
                },
                'line-opacity': 0.5
                
            }
        });
        lines.color.should.deepEqual([
            [10, 'rgba(0, 0, 255, 0.5)'],
            [15, 'rgba(255, 255, 255, 0.5)']
        ]);
    });
    it('text-opacity', function() {
        s = walkStyle({
            "id": "state-label-sm",
            "type": "symbol",
            "layout": {
                "text-size": 14,
                "text-transform": "uppercase",
                "text-font": [
                    "DIN Offc Pro Bold",
                ],
                "text-field": "{name_en}"
            },
            "paint": {
                "text-opacity": 0.5,
                "text-color": "hsl(30, 60%, 60%)",
                "text-halo-color": "hsl(0, 0%, 100%)",
                "text-halo-width": 1
            }
        });
        should.equal(s.font.fill, 'hsla(30, 60%, 60%, 0.5)');
    });
    it('array types with pixels: text-offset', function() {
        s = walkStyle({ type: 'symbol', paint: { 'text-offset': [ 3, -4] }});
        should.deepEqual(s.offset, ['3px', '-4px']);

    });
    it('array types with pixels: icon-translate', function() {
        s = walkStyle({ type: 'symbol', paint: { 'icon-translate': [ 3, -4] }});
        should.deepEqual(s.offset, ['3px', '-4px']);

    });
    it('cross-referencing one style from another', function() {
        let s = toTangram({
            "sources": {
                "composite": {
                    "url": "mapbox://mapbox.mapbox-streets-v7,mapbox.mapbox-terrain-v2",
                    "type": "vector"
                }
            }, "layers": [{
                    "id": "forest",
                    "type": "fill",
                    "source": "composite",
                    "source-layer": "landuse",
                    "filter": [ "==", "class", "wood"],
                    "paint": {
                        "fill-color": "green",
                        "fill-opacity": 1,
                        "fill-antialias": false
                    }
                },
                {
                    "id": "forest2",
                    "ref": "forest",
                    "paint": {
                        "fill-color": "darkgreen",
                    }
                },
            ]
            });
        should.exist(s.styles.forest);
        should.exist(s.styles.forest2);
        should.exist(s.layers.forest);
        should.not.exist(s.layers.forest2);
        should.exist(s.layers.forest.forest2);
        should.exist(s.layers.forest.filter);
        should.not.exist(s.layers.forest.forest2.filter);
        should.deepEqual(s.layers.forest.draw, {forest:{}});
        should.deepEqual(s.layers.forest.forest2.draw, {forest2:{}});
    });

});