/*
This file lets you use the browser's debugger.
*/
/* jshint esnext:true */

var yaml = require('js-yaml');

function showJSON(json, heading, infoboxid) {
    document.getElementById(infoboxid).innerHTML = `<h3>${heading}</h3><pre>` +
        `${JSON.stringify(json, undefined, 2)}`;
}


function initMapbox(style) {
    if (window.mbmap) {
        window.mbmap.setStyle(style);
        return;
    }
    window.mbmap = new mapboxgl.Map({
        container: 'mapboxmap', // container id
        //style: 'in/mapfit-day/style.json',
        style: style,//'mapbox://styles/stevage/cj89fdrvq44x32rp99ugl80fr', // day
        //style: 'mapbox://styles/stevage/cj8ioixs73c072ro168cveriu/', //night
        //style: "compare/in.json",
        center: [-73.993,40.702], // starting position

        zoom: 13.6 // starting zoom
    }).addControl(new mapboxgl.NavigationControl(), 'top-right');

    var popup = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

    window.mbmap.on('click', function (e) {
        var features = window.mbmap.queryRenderedFeatures(e.point);
        var layers = features.map(f => f.layer.id);
        if (layers.length) {
            //var layer = map.getStyle().layers.find(x => x.id === layers[0].id)

            showJSON(features[0].layer, layers[0], 'mapboxinfo');

        } else {
            showJSON({},'','mapboxinfo');
        }

    });
    window.mbmap.on('move', function(e) {
        window.tgmap.setView(window.mbmap.getCenter(), window.mbmap.getZoom()+1);
        document.getElementById('stats').innerHTML='Zoom: ' + window.mbmap.getZoom().toFixed(2);
    });

}

function initMapzen(sceneUri, scene) {
    if (window.tglayer) {
        // already initialised, just update the scene.
        window.tglayer.remove();
        // return;
    } else {      
        window.tgmap = L.map('tangrammap');
    }

    window.tglayer = Tangram.leafletLayer({
        //scene: 'compare/' + (location.hash.slice(1) || 'out') + '.yaml' + '?' + Math.random(),
        scene: sceneUri,
        attribution: '<a href="https://mapzen.com/tangram" target="_blank">Tangram</a> | &copy; OSM contributors | <a href="https://mapzen.com/" target="_blank">Mapzen</a>',
        events: {
            click: e => {
                
                window.tglayer.scene.getFeatureAt(e.pixel)
                    .then(s => {
                        if (s && s.feature && s.feature.layers && s.feature.layers.length) {
                            var layerId = s.feature.layers[0];
                            var renderedLayers = [scene.styles[layerId], scene.layers[layerId]];
                            //var renderedLayers = [];
                            // Object.keys(scene.layers).forEach(l => {
                            //     if (scene.layers[l].data && scene.layers[l].data.layer === layerId)
                            //         renderedLayers.push(scene.layers[l]);
                            // });


                            // //showJSON(s, s.feature.layers[0], 'tangraminfo');
                            //showJSON(s, style.layers[s.feature.layers[0]], 'tangraminfo');
                            showJSON(renderedLayers, layerId, 'tangraminfo');
                            /*document.getElementById('tangraminfo').innerHTML = `<h3></h3>` +
                            `${JSON.stringify(s, undefined, 2).replace(/\n/g, '<br>')}`;*/
                            console.log(s.feature.layers[0]);
                            console.log(s);
                        } else {
                            showJSON({},'','tangraminfo');
                        }
                    });
            }
        },
        selectionRadius: 10
    });

    window.tglayer.addTo(window.tgmap);

    window.tgmap.setView([40.702,-73.993], 13.6);
}

function loadStyle(style) {
    if (!style.layers) {
        alert("Style didn't load properly.");
        return;
    }
    var mb2mz = require('../mb2mz');
    let schemaCross = require('../schemaCross');
    var mzStyle = schemaCross.convertSchema('mapbox', 'mapzen', JSON.parse(JSON.stringify(style)));
    //console.log(mb2mz.toTangram(mzStyle));
    var scene = mb2mz.toTangram(mzStyle);
    var sceneYaml = yaml.safeDump(scene);
    document.getElementById('mapfitstyle').value = sceneYaml;
    //mb2mz.toTangram(mzStyle, true);
    var sceneUri = "data:text/plain;base64," + btoa(sceneYaml);
    initMapzen(sceneUri, scene);
    initMapbox(style);
}

if (location.hash==='#dev') {
    loadStyle(require('../test/mapfit-day.json'));    
} else if (window.location.hash.length > 1) {
    var sourceFile = window.location.hash.slice(1) + '.json';
    fetch(sourceFile).then(r => r.json()).then(loadStyle);
} // else no map

let $mbs = document.getElementById('mapboxstyle');
$mbs.value="";
let $mfs = document.getElementById('mapfitstyle');
$mfs.value="";

mapboxgl.accessToken = 'pk.eyJ1Ijoic3RldmFnZSIsImEiOiJGcW03aExzIn0.QUkUmTGIO3gGt83HiRIjQw';

/*
mapbox://styles/stevage/cj8v1fdr7evbc2ss14h4jlpz3
mapbox://styles/stevage/ciz68fsec00112rpal5hjru07
mapbox://styles/stevage/cj5s42sdy3e1k2sr3yrl98nyg
mapbox://styles/stevage/cj6g6ioot1u6j2sok8suc5lki // error in Mapbox!

mapbox://styles/stevage/cim0e5dap003b9jkpdldte9he // nice
*/

$mbs.addEventListener('change', e => {
    let v = $mbs.value;
    let j;
    if (v.match('^mapbox://')) {
        fetch(v.replace('mapbox://styles/', 'https://api.mapbox.com/styles/v1/') + '?access_token=' + mapboxgl.accessToken).then(r => r.json()).then(loadStyle);
        return;
    }
    try {
        j = JSON.parse(v);
    } catch (e) {
        return;
    }
    // document.getElementById('tangrammap').innerHTML = '';
    loadStyle(j);

});